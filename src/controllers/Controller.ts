import Repository from "../repositories/Repository"

/**
 * makes calls to the repository to process data from the frontend
 */
export default class Controller
{
  repository: Repository;
  constructor()
  {
    this.repository = new Repository()
  }
}
