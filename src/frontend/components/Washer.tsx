import Machine from "./Machine";
import React = require("prop-types/node_modules/@types/react");

export default class Washer extends Machine
{
  constructor(id: number)
  {
    super("W", id)
    this.id = id
  }

  render ()
  {
    return <p>issa washer! number {this.id}</p>
  }
}
