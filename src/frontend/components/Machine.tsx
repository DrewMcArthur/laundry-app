import { Props } from "react";

const React = require("prop-types/node_modules/@types/react")

export default class Machine extends React.Component<{ id: number }>
{
  id: any;
  machineType: string;
  constructor(machineType: string, id: number)
  {
    super({})
    this.machineType = machineType
    this.id = id
  }

  getId ()
  {
    return this.id
  }

  getMachineType ()
  {
    return this.machineType
  }

  getMachineStr ()
  {
    return this.machineType + this.id
  }

  render ()
  {
    return <p>issa machine! {this.getMachineStr()}</p>
  }
}
