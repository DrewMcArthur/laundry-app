import * as React from "react";
import { hot } from "react-hot-loader";
import Controller from "../../controllers/Controller"

const reactLogo = require("./../assets/img/react_logo.svg");
import "./../assets/scss/App.scss";
import Washer from "./Washer"

/**
 * renders home screen, also links to controllers and rest of functionality
 */
class App extends React.Component
{
    controller: Controller;
    constructor()
    {
        super({});
        this.controller = new Controller();
    }
    public render ()
    {
        return (
            <div className="app">
                <h1>Sparhawk St Laundry</h1>
                <div className="machineRow washers">
                    // TODO: create these as elements that show state of machine
                    <Washer id={1} />
                    <Washer id={1} />
                </div>
                <div className="machineRow dryers">
                    {/* <Dryer id={1} />
                    <Dryer id={1} /> */}
                </div>
                {/* <InfoButton /> */}
            </div>
        );
    }
}

declare let module: object;

export default hot(module)(App);
