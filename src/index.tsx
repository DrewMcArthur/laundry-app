import * as React from "react";
import { render } from "react-dom";
import App from "./frontend/components/App";

/**
 * TODO: create one of each of these files for each of the endpoints we want
 */

const rootEl = document.getElementById("root");

render(
    <App />,
    rootEl,
);
